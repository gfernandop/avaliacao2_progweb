package if6ae.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InscricaoMinicursoPK.class)
public abstract class InscricaoMinicursoPK_ {

	public static volatile SingularAttribute<InscricaoMinicursoPK, Integer> numeroInscricao;
	public static volatile SingularAttribute<InscricaoMinicursoPK, Integer> minicurso;

}

