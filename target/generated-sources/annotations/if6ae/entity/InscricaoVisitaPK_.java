package if6ae.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InscricaoVisitaPK.class)
public abstract class InscricaoVisitaPK_ {

	public static volatile SingularAttribute<InscricaoVisitaPK, Integer> numeroInscricao;
	public static volatile SingularAttribute<InscricaoVisitaPK, Integer> visita;

}

