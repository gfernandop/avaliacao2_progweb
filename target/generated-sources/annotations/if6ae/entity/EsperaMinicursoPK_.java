package if6ae.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(EsperaMinicursoPK.class)
public abstract class EsperaMinicursoPK_ {

	public static volatile SingularAttribute<EsperaMinicursoPK, Integer> numeroInscricao;
	public static volatile SingularAttribute<EsperaMinicursoPK, Integer> minicurso;

}

