package if6ae.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InscricaoMinicurso.class)
public abstract class InscricaoMinicurso_ {

	public static volatile SingularAttribute<InscricaoMinicurso, Minicurso> minicurso1;
	public static volatile SingularAttribute<InscricaoMinicurso, Integer> situacao;
	public static volatile SingularAttribute<InscricaoMinicurso, Inscricao> inscricao;
	public static volatile SingularAttribute<InscricaoMinicurso, InscricaoMinicursoPK> inscricaoMinicursoPK;
	public static volatile SingularAttribute<InscricaoMinicurso, Date> dataHora;

}

