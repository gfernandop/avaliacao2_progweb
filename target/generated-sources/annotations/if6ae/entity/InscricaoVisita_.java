package if6ae.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InscricaoVisita.class)
public abstract class InscricaoVisita_ {

	public static volatile SingularAttribute<InscricaoVisita, InscricaoVisitaPK> inscricaoVisitaPK;
	public static volatile SingularAttribute<InscricaoVisita, Integer> situacao;
	public static volatile SingularAttribute<InscricaoVisita, Inscricao> inscricao;
	public static volatile SingularAttribute<InscricaoVisita, Visita> visita1;
	public static volatile SingularAttribute<InscricaoVisita, Date> dataHora;

}

