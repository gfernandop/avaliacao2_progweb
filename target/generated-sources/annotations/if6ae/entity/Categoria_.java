package if6ae.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Categoria.class)
public abstract class Categoria_ {

	public static volatile SingularAttribute<Categoria, Double> valor1Inscricao;
	public static volatile CollectionAttribute<Categoria, Inscricao> inscricaoCollection;
	public static volatile SingularAttribute<Categoria, Integer> codigo;
	public static volatile SingularAttribute<Categoria, Double> valor1Visita;
	public static volatile SingularAttribute<Categoria, Double> valor2Inscricao;
	public static volatile SingularAttribute<Categoria, Double> valor2Minicurso;
	public static volatile SingularAttribute<Categoria, Double> valor2Visita;
	public static volatile SingularAttribute<Categoria, Double> valor1Minicurso;
	public static volatile SingularAttribute<Categoria, String> descricao;

}

