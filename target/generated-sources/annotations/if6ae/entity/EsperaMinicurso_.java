package if6ae.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(EsperaMinicurso.class)
public abstract class EsperaMinicurso_ {

	public static volatile SingularAttribute<EsperaMinicurso, Minicurso> minicurso1;
	public static volatile SingularAttribute<EsperaMinicurso, Inscricao> inscricao;
	public static volatile SingularAttribute<EsperaMinicurso, EsperaMinicursoPK> esperaMinicursoPK;
	public static volatile SingularAttribute<EsperaMinicurso, Boolean> confirmado;
	public static volatile SingularAttribute<EsperaMinicurso, Date> dataHora;

}

