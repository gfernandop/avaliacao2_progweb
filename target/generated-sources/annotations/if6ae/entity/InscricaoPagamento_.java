package if6ae.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(InscricaoPagamento.class)
public abstract class InscricaoPagamento_ {

	public static volatile SingularAttribute<InscricaoPagamento, Date> dataHoraConfirmacao;
	public static volatile SingularAttribute<InscricaoPagamento, Date> dataPagamento;
	public static volatile SingularAttribute<InscricaoPagamento, Inscricao> numeroInscricao;
	public static volatile SingularAttribute<InscricaoPagamento, Date> dataRetorno;
	public static volatile SingularAttribute<InscricaoPagamento, Date> dataHoraProcessamento;
	public static volatile SingularAttribute<InscricaoPagamento, Date> dataVencimento;
	public static volatile SingularAttribute<InscricaoPagamento, Double> valorPago;
	public static volatile SingularAttribute<InscricaoPagamento, Double> valorTarifa;
	public static volatile SingularAttribute<InscricaoPagamento, Integer> numeroDocumento;
	public static volatile SingularAttribute<InscricaoPagamento, Integer> bancoPagamento;
	public static volatile SingularAttribute<InscricaoPagamento, Double> valorDocumento;

}

