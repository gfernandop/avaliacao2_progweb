package if6ae.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Minicurso.class)
public abstract class Minicurso_ {

	public static volatile SingularAttribute<Minicurso, Integer> codigo;
	public static volatile SingularAttribute<Minicurso, Integer> vagas;
	public static volatile CollectionAttribute<Minicurso, EsperaMinicurso> esperaMinicursoCollection;
	public static volatile CollectionAttribute<Minicurso, InscricaoMinicurso> inscricaoMinicursoCollection;
	public static volatile SingularAttribute<Minicurso, String> ministrante;
	public static volatile SingularAttribute<Minicurso, String> descricao;

}

