package if6ae.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Visita.class)
public abstract class Visita_ {

	public static volatile SingularAttribute<Visita, Integer> codigo;
	public static volatile SingularAttribute<Visita, Integer> vagas;
	public static volatile SingularAttribute<Visita, String> descricao;
	public static volatile CollectionAttribute<Visita, InscricaoVisita> inscricaoVisitaCollection;

}

