package if6ae.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Inscricao.class)
public abstract class Inscricao_ {

	public static volatile SingularAttribute<Inscricao, String> cidade;
	public static volatile SingularAttribute<Inscricao, String> estado;
	public static volatile SingularAttribute<Inscricao, Integer> situacao;
	public static volatile SingularAttribute<Inscricao, Integer> numero;
	public static volatile SingularAttribute<Inscricao, String> endereco;
	public static volatile SingularAttribute<Inscricao, String> bairro;
	public static volatile SingularAttribute<Inscricao, Categoria> categoria;
	public static volatile SingularAttribute<Inscricao, String> nome;
	public static volatile CollectionAttribute<Inscricao, EsperaMinicurso> esperaMinicursoCollection;
	public static volatile CollectionAttribute<Inscricao, InscricaoMinicurso> inscricaoMinicursoCollection;
	public static volatile SingularAttribute<Inscricao, Integer> cep;
	public static volatile CollectionAttribute<Inscricao, InscricaoVisita> inscricaoVisitaCollection;
	public static volatile SingularAttribute<Inscricao, String> fone;
	public static volatile SingularAttribute<Inscricao, String> atuacaoEmpresa;
	public static volatile CollectionAttribute<Inscricao, InscricaoPagamento> inscricaoPagamentoCollection;
	public static volatile SingularAttribute<Inscricao, String> complemento;
	public static volatile SingularAttribute<Inscricao, Long> cpf;
	public static volatile SingularAttribute<Inscricao, Boolean> participacao;
	public static volatile SingularAttribute<Inscricao, Date> dataHora;
	public static volatile SingularAttribute<Inscricao, String> complementoCategoria;
	public static volatile SingularAttribute<Inscricao, String> email;

}

