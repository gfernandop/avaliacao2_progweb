/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if6ae.jpa;

import if6ae.entity.Inscricao;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author gab
 */
public class InscricaoJpa {
    
    public EntityManagerFactory emf;
    public List<Inscricao> inscricoes;
    
    public InscricaoJpa(){
        this.emf = Persistence.createEntityManagerFactory("utfpr.ct.dainf.if6ae.avaliacao_avaliacao2-2015-1_war_1.0.0-SNAPSHOTPU");
        EntityManager em = emf.createEntityManager();
        inscricoes = em.createNamedQuery("Inscricao.findByNumero").getResultList();
        
    }
    
    public List<Inscricao> findByNumero(int numero){
        /*
        EntityManager em = emf.createEntityManager();
        TypedQuery<Inscricao> q = em.createNamedQuery("Inscricao.findByNumero", Inscricao.class);
        q.setParameter(numero, q);
        List<Inscricao> inscricoes = q.getResultList();
        */
        EntityManager em = emf.createEntityManager();
        List<Inscricao> inscricoes = em.createNamedQuery("Inscricao.findByNumero").getResultList();
        return inscricoes;
    }
    
    public List<Inscricao> findByCPF(long cpf){
        /*
        EntityManager em = emf.createEntityManager();
        TypedQuery<Inscricao> q = em.createNamedQuery("Inscricao.findByCpf", Inscricao.class);
        q.setParameter((int) cpf, q);
        List<Inscricao> inscricoes = q.getResultList();
        return inscricoes;
        */
        EntityManager em = emf.createEntityManager();
        List<Inscricao> inscricoes = em.createNamedQuery("Inscricao.findByCpf").getResultList();
        return inscricoes;
    }

    public List<Inscricao> getInscricoes() {
        return inscricoes;
    }

    public void setInscricoes(List<Inscricao> inscricoes) {
        this.inscricoes = inscricoes;
    }
    
    
    
}
    